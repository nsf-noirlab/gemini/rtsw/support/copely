# Welcome to the EPICS Interface for the Copely Controller
This support module is intended to implement an interface to the Copely controler.
This python code verifies that we can talk to the controler over serial with udp.

```python
from socket import *

s = socket(type=SOCK_DGRAM)
s.sendto(b'g r0x20\r\n',('10.1.11.78',19658))
data,addr = s.recvfrom(1024)
print(data,addr)
```

Find new commands in the documents.

